FROM sutty/rust:latest AS build
MAINTAINER "f <f@sutty.nl>"

RUN cargo install --version 2.3.0 oxipng
RUN strip --strip-all /root/.cargo/bin/oxipng

FROM alpine:3.11.6
COPY --from=build /root/.cargo/bin/oxipng /usr/bin/oxipng
